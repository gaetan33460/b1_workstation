# =======================================================
# NAME: script1.ps1
# AUTHOR: ROUX GAËTAN, Ynov
# DATE: 19/10/2020
# COMMENTS : - afficher un résumé de l'OS
#            - liste les utilisateurs de la machine 
#            - calcule et affiche le temps de réponse moyen vers 8.8.8.8
# =======================================================


# Affiche un résumé de l'OS

# affiche le nom de la machine 
Write-host "Nom du poste : $env:COMPUTERNAME"

# affiche IP principale 
Write-host "IP principale :"((ipconfig | findstr [0-9].\.)[0]).Split()[-1]

# OS et version de l'OS
Write-Host "OS : "(Get-WmiObject -class Win32_OperatingSystem).Caption
Write-Host "Version : "(Get-WmiObject -class Win32_OperatingSystem).Version

# Date et heure d'allumage
Write-Host "Date et Heure d'allumage :"(Get-CimInstance -ClassName Win32_OperatingSystem).LastBootUpTime

# Détermine si l'OS est à jour
$ajour = wmic OS get status
Write-Host "determiner si l'OS est a jour :" $ajour

# Espace RAM utilisé / Espace RAM dispo

$os = Get-WmiObject Win32_OperatingSystem
    $RAM_utilise = [math]::Round(($os.TotalVisibleMemorySize-$os.FreePhysicalMemory) / 1000000, 2)
    $RAM_libre = [math]::Round((Get-WmiObject win32_operatingsystem).FreePhysicalMemory / 1000000, 2)
    Write-Output "La RAM utilise est de : $RAM_utilise Mo"
    Write-Output "La RAM libre est de : $RAM_libre Mo"


# Espace disque utilisé / Espace disque dispo

$os =  Get-WmiObject Win32_logicaldisk
foreach($element in $os){
        $stockage_utilise = [math]::Round($element.Size/1000000000,2)
        $stockage_libre = [math]::Round($element.FreeSpace/1000000000,2)
        Write-Output "Le stockage utilise est de : $stockage_utilise Go"
        Write-Output "Le stockage libre est de : $stockage_libre Go"
    }

# Liste les utilisateurs de la machine
$utlisateur = Get-LocalUser
Write-Output "Liste les utilisateurs de la machine :"$utlisateur

# calcule et affiche le temps de réponse moyen vers 8.8.8.8, ping

$ping = Test-Connection -count 10 10.33.3.253
    $moyenne = ($ping | Measure-Object ResponseTime -average -maximum)
    $calcul = [math]::Round($moyenne.average, 2)
    Write-Output "Le temps moyens vers 10.33.3.253 est de $calcul"



