# =======================================================
# NAME: script2.ps1
# AUTHOR: ROUX GAËTAN, Ynov
# DATE: 19/10/2020
# COMMENTS : - exécuter une action
#            - lock l'écran après X secondes
#            - éteindre le PC après X secondes
# =======================================================

# lock l'écran après X secondes 
$valeur = Read-Host "Temps d'attente avant l'execution du script"
Start-Sleep -Seconds $valeur
rundll32.exe user32.dll,LockWorkStation






# éteind le PC après X secondes 
$eteind = shutdown /s /t 75
Write-Host "L'ordinateur vas s'eteindre dans 75 secondes"$eteind