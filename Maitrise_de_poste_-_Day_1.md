
# Maitrise de poste - Day 1

## Host OS

* nom de la machine :
```
C:\Users\gaeta\Documents\b1_workstation>hostname
DESKTOP-L7KLVI6
```
* OS et version :
```
C:\Users\gaeta\Documents\b1_workstation>ver
Microsoft Windows Famille [version 10.0.19041.508]
```
* architecture processeur (32-bit, 64-bit, ARM, etc)
```
C:\Users\gaeta\Documents\b1_workstation>systeminfo

[...]
Type du système: 
       x64-based PC
[...]
```
* quantité RAM et modèle de la RAM
```
PS C:\Users\gaeta> Get-CimInstance -ClassName Win32_PhysicalMemory


[...]
Model  :
[...]
```
```
C:\Users\gaeta\Documents\b1_workstation>systeminfo

[...]
Mémoire physique totale    8 094 Mo
[...]
```

## Devices

* la marque et le modèle de votre processeur
```
C:\Users\gaeta\Documents\b1_workstation>wmic cpu get caption, name
Caption                               Name
Intel64 Family 6 Model 61 Stepping 4  Intel(R) Core(TM) i5-5200U CPU @ 2.20GHz
```
* identifier le nombre de processeurs, le nombre de coeur
```
C:\Users\gaeta\Documents\b1_workstation>WMIC CPU Get DeviceID,NumberOfCores,NumberOfLogicalProcessors
DeviceID  NumberOfCores  NumberOfLogicalProcessors
CPU0      2              4
```
  * si c'est un proc Intel, expliquer le nom du processeur
```
IntelCore : marque
i5 : generation du processeur (5 eme)
5200U : modele 
[...]
```
* la marque et le modèle :
  * de votre touchpad/trackpad
```
PS C:\Users\gaeta> Get-CimInstance win32_POINTINGDEVICE | select hardwaretype
>>

hardwaretype
------------
Souris HID
ASUS Touchpad 
```
  * de votre carte graphique
```
C:\Users\gaeta\Documents\b1_workstation>wmic path win32_VideoController get name
Name
Intel(R) HD Graphics 5500
NVIDIA GeForce GTX 950M
```
* identifier la marque et le modèle de votre(vos) disque(s) dur(s)
```
C:\Users\gaeta\Documents\b1_workstation>wmic diskdrive get model
Model
CT250MX500SSD4
HGST HTS541010A7E630
```
* identifier les différentes partitions de votre/vos disque(s) dur(s)
```
DISKPART> select disk 0

Le disque 0 est maintenant le disque sélectionné.

DISKPART> list partition

  N° partition   Type              Taille   Décalage
  -------------  ----------------  -------  --------
  Partition 1    Système            260 M   1024 K
  Partition 2    Réservé             16 M    261 M
  Partition 3    Principale         931 G    277 M
```
```
DISKPART> select disk 1

Le disque 1 est maintenant le disque sélectionné.

DISKPART> list partition

  N° partition   Type              Taille   Décalage
  -------------  ----------------  -------  --------
  Partition 1    Réservé            128 M     17 K
  Partition 2    Système            260 M    129 M
  Partition 3    Principale         231 G    389 M
  Partition 4    Récupération       591 M    232 G
```
* déterminer le système de fichier de chaque partition
```
DISKPART> select disk 0

Le disque 0 est maintenant le disque sélectionné.

DISKPART> list disk

  N° disque  Statut         Taille   Libre    Dyn  GPT
  ---------  -------------  -------  -------  ---  ---
* Disque 0    En ligne        931 G octets  1024 K octets        *
  Disque 1    En ligne        232 G octets  2048 K octets        *

DISKPART> list volume

  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
  Volume 0     D   DATAS        NTFS   Partition    931 G   Sain
  Volume 1         SYSTEM       FAT32  Partition    260 M   Sain       Masqué
  Volume 2     C   OS           NTFS   Partition    231 G   Sain       Démarrag
  Volume 3                      NTFS   Partition    591 M   Sain
  Volume 4         SYSTEM       FAT32  Partition    260 M   Sain       Système
```
* expliquer la fonction de chaque partition
```
NTFS :système de fichiers permettant d'organiser les données enregistrées dans une mémoire comme un disque dur
FAT32 : table d’allocation de fichiers
```
## Users  
 Déterminer la liste des utilisateurs de la machine
* la liste **complète** des utilisateurs de la machine (je vous vois les Windowsiens...)
```
C:\Users\gaeta>net user

comptes d’utilisateurs de \\DESKTOP-L7KLVI6

-------------------------------------------------------------------------------
Administrateur           DefaultAccount           gaeta
Invité                   WDAGUtilityAccount
La commande s’est terminée correctement.
```
* déterminer le nom de l'utilisateur qui est full admin sur la machine
  * il existe toujours un utilisateur particulier qui a le droit de tout faire sur la machine
  * pour les Windowsiens : faites des recherches sur `NT-AUTHORITY\SYSTEM` et le concept de SID
```
C:\Users\gaeta>wmic useraccount list full


AccountType=512
Description=Compte d'utilisateur d'administration
Disabled=TRUE
Domain=DESKTOP-L7KLVI6
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=Administrateur
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-2888478459-493145096-3486497455-500
SIDType=1
Status=Degraded


AccountType=512
Description=Compte utilisateur géré par le système.
Disabled=TRUE
Domain=DESKTOP-L7KLVI6
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=DefaultAccount
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=FALSE
SID=S-1-5-21-2888478459-493145096-3486497455-503
SIDType=1
Status=Degraded


AccountType=512
Description=
Disabled=FALSE
Domain=DESKTOP-L7KLVI6
FullName=Gaetan ROUX
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=gaeta
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-2888478459-493145096-3486497455-1001
SIDType=1
Status=OK

[...]
```

## Processus
* Déterminer la liste des processus de la machine
```
C:\Users\gaeta>tasklist

Nom de l’image                 PID Nom de la sessio Numéro de s Utilisation
========================= ======== ================ =========== ============
System Idle Process              0 Services                   0         8 Ko
System                           4 Services                   0     4 508 Ko
Registry                       100 Services                   0    66 120 Ko
smss.exe                       580 Services                   0     1 156 Ko
csrss.exe                      804 Services                   0     5 768 Ko
wininit.exe                    636 Services                   0     7 012 Ko
services.exe                   904 Services                   0    11 692 Ko
lsass.exe                      920 Services                   0    23 228 Ko
svchost.exe                   1012 Services                   0     3 060 Ko
fontdrvhost.exe               1032 Services                   0     3 560 Ko
svchost.exe                   1048 Services                   0    44 948 Ko
WUDFHost.exe                  1088 Services                   0    12 984 Ko
svchost.exe                   1156 Services                   0    23 840 Ko
svchost.exe                   1232 Services                   0     9 040 Ko
svchost.exe                   1500 Services                   0    10 584 Ko
svchost.exe                   1508 Services                   0    12 248 Ko
svchost.exe                   1596 Services                   0     8 224 Ko
svchost.exe                   1620 Services                   0     6 108 Ko
svchost.exe                   1652 Services                   0     7 712 Ko
svchost.exe                   1792 Services                   0     9 672 Ko
svchost.exe                   1864 Services                   0     6 000 Ko
igfxCUIService.exe            1900 Services                   0    10 136 Ko
NVDisplay.Container.exe       2016 Services                   0    17 452 Ko
svchost.exe                    664 Services                   0    18 148 Ko
svchost.exe                   1404 Services                   0    12 764 Ko
svchost.exe                   1896 Services                   0    20 676 Ko
svchost.exe                   2072 Services                   0    21 136 Ko
svchost.exe                   2108 Services                   0     6 804 Ko
svchost.exe                   2188 Services                   0     9 752 Ko
svchost.exe                   2224 Services                   0    13 268 Ko
svchost.exe                   2240 Services                   0    13 592 Ko
svchost.exe                   2268 Services                   0     5 452 Ko
svchost.exe                   2400 Services                   0     7 808 Ko
svchost.exe                   2408 Services                   0     8 976 Ko
svchost.exe                   2444 Services                   0     7 480 Ko
Memory Compression            2452 Services                   0   162 488 Ko
svchost.exe                   2512 Services                   0    11 604 Ko
svchost.exe                   2540 Services                   0    17 420 Ko
svchost.exe                   2700 Services                   0    13 088 Ko
svchost.exe                   2964 Services                   0    10 268 Ko
svchost.exe                   3084 Services                   0    18 904 Ko
svchost.exe                   3308 Services                   0     9 924 Ko
svchost.exe                   3324 Services                   0     6 244 Ko
svchost.exe                   3336 Services                   0     9 556 Ko
svchost.exe                   3540 Services                   0    13 188 Ko
svchost.exe                   3700 Services                   0    19 960 Ko
AsLdrSrv.exe                  3872 Services                   0     6 272 Ko
svchost.exe                   3880 Services                   0    14 584 Ko
GFNEXSrv.exe                  3924 Services                   0     3 768 Ko
spoolsv.exe                   4028 Services                   0    12 868 Ko
svchost.exe                   4072 Services                   0    19 024 Ko
svchost.exe                   2360 Services                   0     7 488 Ko
coreServiceShell.exe          4120 Services                   0   354 396 Ko
uiWatchDog.exe                4164 Services                   0     1 176 Ko
ServiceMiseAJourIndex.exe     4172 Services                   0    16 216 Ko
conhost.exe                   4196 Services                   0     7 872 Ko
coreFrameworkHost.exe         4260 Services                   0     6 732 Ko
conhost.exe                   4268 Services                   0     7 840 Ko
AdminService.exe              4468 Services                   0     7 444 Ko
schedul2.exe                  4476 Services                   0    12 444 Ko
mDNSResponder.exe             4484 Services                   0     7 180 Ko
svchost.exe                   4504 Services                   0     8 820 Ko
AdobeUpdateService.exe        4520 Services                   0     7 100 Ko
OfficeClickToRun.exe          4528 Services                   0    49 136 Ko
svchost.exe                   4560 Services                   0    14 800 Ko
svchost.exe                   4632 Services                   0     8 344 Ko
svchost.exe                   4640 Services                   0    41 140 Ko
esif_uf.exe                   4680 Services                   0     7 588 Ko
svchost.exe                   4692 Services                   0    31 896 Ko
svchost.exe                   4720 Services                   0    10 140 Ko
HuaweiHiSuiteService64.ex     4728 Services                   0    10 428 Ko
svchost.exe                   4744 Services                   0    10 652 Ko
svchost.exe                   4860 Services                   0     8 580 Ko
nvcontainer.exe               4872 Services                   0    30 236 Ko
PtSvcHost.exe                 4944 Services                   0   123 464 Ko
svchost.exe                   5036 Services                   0     9 624 Ko
svchost.exe                   5048 Services                   0     5 972 Ko
svchost.exe                   5084 Services                   0     5 504 Ko
svchost.exe                   5108 Services                   0    22 864 Ko
PtWatchDog.exe                4372 Services                   0     5 776 Ko
svchost.exe                   5312 Services                   0    11 040 Ko
dasHost.exe                   5324 Services                   0    11 692 Ko
svchost.exe                   5484 Services                   0     5 168 Ko
svchost.exe                   5664 Services                   0    11 740 Ko
unsecapp.exe                  5756 Services                   0     8 284 Ko
svchost.exe                   5872 Services                   0     9 020 Ko
svchost.exe                   5892 Services                   0    10 428 Ko
svchost.exe                   6008 Services                   0     7 356 Ko
PresentationFontCache.exe     7848 Services                   0    15 984 Ko
svchost.exe                   7860 Services                   0    37 548 Ko
svchost.exe                   7924 Services                   0    20 412 Ko
svchost.exe                   7992 Services                   0     8 080 Ko
dllhost.exe                   8064 Services                   0    10 920 Ko
svchost.exe                   8348 Services                   0     8 544 Ko
svchost.exe                   8932 Services                   0    22 508 Ko
svchost.exe                   9004 Services                   0     9 588 Ko
svchost.exe                   9304 Services                   0    20 996 Ko
svchost.exe                   9644 Services                   0     7 708 Ko
svchost.exe                   9952 Services                   0    12 616 Ko
svchost.exe                  10184 Services                   0    13 300 Ko
svchost.exe                  10948 Services                   0    16 012 Ko
svchost.exe                  11856 Services                   0    13 180 Ko
TmsaInstance64.exe           11964 Services                   0    24 100 Ko
SecurityHealthService.exe    12408 Services                   0    17 120 Ko
DrSDKCaller.exe              15072 Services                   0    22 264 Ko
conhost.exe                  11564 Services                   0     5 028 Ko
svchost.exe                  15864 Services                   0    25 300 Ko
DiscSoftBusServiceLite.ex    16340 Services                   0    14 816 Ko
svchost.exe                   6020 Services                   0    11 708 Ko
jhi_service.exe              17460 Services                   0     6 996 Ko
LMS.exe                      17612 Services                   0    11 848 Ko
RichVideo64.exe              18264 Services                   0     6 872 Ko
SgrmBroker.exe               18348 Services                   0     9 480 Ko
TeamViewer_Service.exe       15852 Services                   0    19 784 Ko
svchost.exe                  18344 Services                   0    11 152 Ko
svchost.exe                   2312 Services                   0    18 692 Ko
SearchIndexer.exe             3820 Services                   0    55 248 Ko
armsvc.exe                    7748 Services                   0     6 580 Ko
iPodService.exe               1000 Services                   0     9 028 Ko
AppleMobileDeviceService.    17880 Services                   0    14 888 Ko
isa.exe                        344 Services                   0    21 232 Ko
svchost.exe                   4112 Services                   0    18 208 Ko
rundll32.exe                  6612 Services                   0    11 068 Ko
svchost.exe                   4648 Services                   0     4 800 Ko
SetupSync.exe                14472 Services                   0    12 304 Ko
conhost.exe                  10580 Services                   0     8 360 Ko
cmd.exe                       4344 Services                   0     5 296 Ko
conhost.exe                  16504 Services                   0     8 384 Ko
msiexec.exe                  10380 Services                   0    16 156 Ko
msiexec.exe                   1084 Services                   0    10 312 Ko
BatchCaller.exe              17688 Services                   0    11 444 Ko
svchost.exe                   3440 Services                   0    22 920 Ko
rundll32.exe                 14512 Services                   0    11 056 Ko
svchost.exe                  17756 Services                   0    11 920 Ko
svchost.exe                  14000 Services                   0     9 092 Ko
rundll32.exe                  3232 Services                   0    11 080 Ko
rundll32.exe                 11072 Services                   0    11 104 Ko
svchost.exe                  17228 Services                   0    12 000 Ko
WmiPrvSE.exe                 14192 Services                   0    22 212 Ko
rundll32.exe                  8964 Services                   0    11 096 Ko
rundll32.exe                  9396 Services                   0    11 096 Ko
AGSService.exe                7536 Services                   0    14 840 Ko
rundll32.exe                  6484 Services                   0    11 060 Ko
rundll32.exe                   844 Services                   0    11 088 Ko
rundll32.exe                 11920 Services                   0    11 100 Ko
rundll32.exe                 12144 Services                   0    11 080 Ko
svchost.exe                   9964 Services                   0     8 100 Ko
svchost.exe                  10844 Services                   0     7 504 Ko
svchost.exe                   6676 Services                   0    10 044 Ko
rundll32.exe                  9624 Services                   0    11 048 Ko
svchost.exe                   2324 Services                   0     5 600 Ko
rundll32.exe                 15532 Services                   0    11 068 Ko
svchost.exe                  19648 Services                   0     6 116 Ko
csrss.exe                    20848 Console                   14     7 632 Ko
winlogon.exe                 19992 Console                   14    14 852 Ko
fontdrvhost.exe               8344 Console                   14     7 028 Ko
dwm.exe                       8592 Console                   14   115 492 Ko
NVDisplay.Container.exe      20160 Console                   14    46 396 Ko
rundll32.exe                 15912 Services                   0    12 676 Ko
HControl.exe                  3292 Console                   14    10 348 Ko
esif_assist_64.exe            8892 Console                   14     4 564 Ko
nvcontainer.exe               9204 Console                   14    46 544 Ko
sihost.exe                   15396 Console                   14    26 888 Ko
svchost.exe                   2432 Console                   14    29 592 Ko
svchost.exe                  18152 Console                   14    43 868 Ko
taskhostw.exe                19480 Console                   14    22 816 Ko
ACMON.exe                    18488 Console                   14     3 444 Ko
USBChargerPlus.exe           15640 Console                   14     3 668 Ko
WpcMon.exe                   21432 Console                   14    16 772 Ko
igfxEM.exe                   13904 Console                   14    14 708 Ko
igfxHK.exe                    9284 Console                   14    11 276 Ko
ATKOSD2.exe                  15352 Console                   14    15 616 Ko
DMedia.exe                    3452 Console                   14     8 432 Ko
igfxTray.exe                  9924 Console                   14    12 896 Ko
explorer.exe                 18020 Console                   14   144 092 Ko
ctfmon.exe                   12120 Console                   14    22 412 Ko
svchost.exe                  14492 Services                   0     8 624 Ko
svchost.exe                  16896 Console                   14    32 100 Ko
svchost.exe                  14088 Console                   14    24 128 Ko
WUDFHost.exe                  8548 Services                   0    11 336 Ko
StartMenuExperienceHost.e     4676 Console                   14    87 424 Ko
RuntimeBroker.exe            18124 Console                   14    28 856 Ko
RuntimeBroker.exe            16508 Console                   14    40 412 Ko
YourPhone.exe                10132 Console                   14    44 864 Ko
SettingSyncHost.exe          14556 Console                   14     5 828 Ko
NVIDIA Web Helper.exe         8404 Console                   14     4 064 Ko
conhost.exe                  12444 Console                   14     1 092 Ko
AsusTPLoader.exe             14704 Console                   14     2 392 Ko
chrome.exe                    8212 Console                   14   182 212 Ko
chrome.exe                    9476 Console                   14     7 632 Ko
chrome.exe                   20052 Console                   14   180 980 Ko
chrome.exe                   19620 Console                   14    47 548 Ko
RAVCpl64.exe                  7292 Console                   14     3 704 Ko
RAVBg64.exe                   4764 Console                   14     4 724 Ko
chrome.exe                    3464 Console                   14    52 708 Ko
SecurityHealthSystray.exe    13560 Console                   14     9 924 Ko
chrome.exe                   16220 Console                   14    58 984 Ko
OUTLOOK.EXE                  18532 Console                   14   363 712 Ko
chrome.exe                   10712 Console                   14   136 504 Ko
TiltWheelMouse.exe             924 Console                   14     9 380 Ko
chrome.exe                   16016 Console                   14    74 044 Ko
chrome.exe                    1568 Console                   14    54 244 Ko
schedhlp.exe                 14744 Console                   14     9 740 Ko
TextInputHost.exe             3132 Console                   14    45 600 Ko
dllhost.exe                  10236 Console                   14    13 212 Ko
PtSessionAgent.exe           20196 Console                   14    12 884 Ko
Skype.exe                     7176 Console                   14    71 964 Ko
SearchApp.exe                20424 Console                   14    90 276 Ko
cmd.exe                       1468 Console                   14     4 196 Ko
conhost.exe                  21076 Console                   14    12 984 Ko
hpwuschd2.exe                 8132 Console                   14     6 516 Ko
ToolbarNativeMsgHost.exe      7428 Console                   14    13 556 Ko
TibMounterMonitor.exe        19932 Console                   14     6 860 Ko
TrueImageMonitor.exe         18760 Console                   14    34 752 Ko
Discord.exe                  10344 Console                   14    70 940 Ko
Skype.exe                    19776 Console                   14    26 748 Ko
Discord.exe                  14076 Console                   14    86 816 Ko
Skype.exe                     8688 Console                   14    53 532 Ko
Skype.exe                    20892 Console                   14    35 100 Ko
Discord.exe                   9836 Console                   14    39 412 Ko
Skype.exe                    14376 Console                   14   103 724 Ko
Discord.exe                  16036 Console                   14    52 816 Ko
Discord.exe                   8472 Console                   14   244 012 Ko
WmiPrvSE.exe                 19604 Services                   0     9 532 Ko
Discord.exe                  16764 Console                   14    58 160 Ko
DTShellHlp.exe                4272 Console                   14    19 560 Ko
chrome.exe                   10704 Console                   14    70 212 Ko
chrome.exe                   20324 Console                   14    98 888 Ko
RuntimeBroker.exe             1488 Console                   14    15 616 Ko
MoUsoCoreWorker.exe           3260 Services                   0    46 028 Ko
git-cmd.exe                  18656 Console                   14     5 172 Ko
conhost.exe                   2916 Console                   14    20 120 Ko
cmd.exe                      10404 Console                   14     4 436 Ko
svchost.exe                   3224 Console                   14     9 524 Ko
ShellExperienceHost.exe      14084 Console                   14    54 624 Ko
RuntimeBroker.exe            21464 Console                   14    29 252 Ko
SystemSettingsBroker.exe     20576 Console                   14    35 752 Ko
SystemSettings.exe           14280 Console                   14    67 468 Ko
ApplicationFrameHost.exe      5632 Console                   14    31 184 Ko
GameBarPresenceWriter.exe     1764 Console                   14    17 344 Ko
svchost.exe                  13764 Services                   0    15 168 Ko
chrome.exe                   10280 Console                   14   115 020 Ko
chrome.exe                    7632 Console                   14    16 328 Ko
audiodg.exe                  19632 Services                   0    27 724 Ko
chrome.exe                   14644 Console                   14   101 160 Ko
chrome.exe                    1716 Console                   14    73 400 Ko
chrome.exe                    3204 Console                   14    59 136 Ko
chrome.exe                     684 Console                   14    52 012 Ko
chrome.exe                    3120 Console                   14    56 024 Ko
chrome.exe                     616 Console                   14    54 068 Ko
chrome.exe                   14784 Console                   14    84 128 Ko
chrome.exe                   17184 Console                   14    67 528 Ko
chrome.exe                    6556 Console                   14    54 200 Ko
svchost.exe                  19204 Services                   0    11 716 Ko
svchost.exe                  15968 Services                   0     9 192 Ko
svchost.exe                  16408 Services                   0     8 764 Ko
svchost.exe                  16492 Services                   0    12 196 Ko
RuntimeBroker.exe             1552 Console                   14    15 092 Ko
smartscreen.exe              10988 Console                   14     8 156 Ko
chrome.exe                    1932 Console                   14   125 836 Ko
chrome.exe                    7356 Console                   14    22 852 Ko
WmiPrvSE.exe                 14004 Services                   0     9 360 Ko
chrome.exe                   14708 Console                   14    43 268 Ko
tasklist.exe                 21484 Console                   14     9 404 Ko
```
* choisissez 5 services système et expliquer leur utilité
```
audiodg.exe : Permet aux utilisateurs d'écouter des fichiers audio sans aucune interruption et gère également la gestion des droits numériques
WmiPrvSE.exe : Permet aux applications installées sur votre machine de demander des informations sur votre système
cmd.exe :c'est un processus générique de Windows ouvrant une console en mode texte permettant de lancer des applications à l'aide de commandes
rundll32 : Sert à charger les librairies dynamiques en mémoire afin de les rendre utilisables par d'autres programmes
msiexec.exe : Sert à installer, réparer et supprimer des programmes fournis en packages Windows Installer
```
* déterminer les processus lancés par l'utilisateur qui est full admin sur la machine
```
## A chercher 
```

## Network

Afficher la liste des cartes réseau de votre machine
```
PS C:\WINDOWS\system32> Get-NetAdapter | fl Name, InterfaceIndex


Name           : Wi-Fi
InterfaceIndex : 18

Name           : Ethernet
InterfaceIndex : 16
```
* expliquer la fonction de chacune d'entre elles
```
Carte Wifi :La carte wifi est une norme de communication permettant la transmission de données numériques sans fil
Carte Ethernet : La carte réseau constitue l’interface entre l’ordinateur et le câble du réseau. La fonction d’une carte réseau est de préparer, d’envoyer et de contrôler les données sur le réseau
```



Lister tous les ports TCP et UDP en utilisation
* déterminer quel programme tourne derrière chacun des ports
```
C:\WINDOWS\system32>netstat -ab

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:135            DESKTOP-L7KLVI6:0      LISTENING
  RpcSs
 [svchost.exe]
  TCP    0.0.0.0:445            DESKTOP-L7KLVI6:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:5040           DESKTOP-L7KLVI6:0      LISTENING
  CDPSvc
 [svchost.exe]
  TCP    0.0.0.0:7680           DESKTOP-L7KLVI6:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:45769          DESKTOP-L7KLVI6:0      LISTENING
 [DiscSoftBusServiceLite.exe]
  TCP    0.0.0.0:49664          DESKTOP-L7KLVI6:0      LISTENING
 [lsass.exe]
  TCP    0.0.0.0:49665          DESKTOP-L7KLVI6:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49666          DESKTOP-L7KLVI6:0      LISTENING
  Schedule
 [svchost.exe]
  TCP    0.0.0.0:49667          DESKTOP-L7KLVI6:0      LISTENING
  EventLog
 [svchost.exe]
  TCP    0.0.0.0:49668          DESKTOP-L7KLVI6:0      LISTENING
 [spoolsv.exe]
  TCP    0.0.0.0:49673          DESKTOP-L7KLVI6:0      LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:5354         DESKTOP-L7KLVI6:0      LISTENING
 [mDNSResponder.exe]
  TCP    127.0.0.1:5354         DESKTOP-L7KLVI6:49669  ESTABLISHED
 [mDNSResponder.exe]
  TCP    127.0.0.1:5354         DESKTOP-L7KLVI6:49688  ESTABLISHED
 [mDNSResponder.exe]
  TCP    127.0.0.1:5939         DESKTOP-L7KLVI6:0      LISTENING
 [TeamViewer_Service.exe]
  TCP    127.0.0.1:6463         DESKTOP-L7KLVI6:0      LISTENING
 [Discord.exe]
  TCP    127.0.0.1:27015        DESKTOP-L7KLVI6:0      LISTENING
 [AppleMobileDeviceService.exe]
  TCP    127.0.0.1:37848        DESKTOP-L7KLVI6:0      LISTENING
 [coreServiceShell.exe]
  TCP    127.0.0.1:49669        DESKTOP-L7KLVI6:5354   ESTABLISHED
 [AppleMobileDeviceService.exe]
  TCP    127.0.0.1:49688        DESKTOP-L7KLVI6:5354   ESTABLISHED
 [AppleMobileDeviceService.exe]
  TCP    127.0.0.1:61294        DESKTOP-L7KLVI6:65001  ESTABLISHED
 [nvcontainer.exe]
  TCP    127.0.0.1:61547        DESKTOP-L7KLVI6:0      LISTENING
 [NVIDIA Web Helper.exe]
  TCP    127.0.0.1:65001        DESKTOP-L7KLVI6:0      LISTENING
 [nvcontainer.exe]
[...]
 [SearchApp.exe]
  TCP    [2a01:e34:ef74:c9e0:a115:871a:4ea9:bbeb]:63521  [2606:2800:133:206e:1315:22a5:2006:24fd]:https  CLOSE_WAIT
 [SearchApp.exe]
  TCP    [2a01:e34:ef74:c9e0:a115:871a:4ea9:bbeb]:63522  [2606:2800:133:206e:1315:22a5:2006:24fd]:https  CLOSE_WAIT
 [SearchApp.exe]
  TCP    [2a01:e34:ef74:c9e0:a115:871a:4ea9:bbeb]:63541  g2a02-26f0-2f00-03a7-0000-0000-0000-097a:https  CLOSE_WAIT
 [coreServiceShell.exe]
  TCP    [2a01:e34:ef74:c9e0:a115:871a:4ea9:bbeb]:63550  [2406:da14:88d:a102:98e7:a51a:ff3a:dcc1]:https  ESTABLISHED
 [chrome.exe]
  TCP    [2a01:e34:ef74:c9e0:a115:871a:4ea9:bbeb]:63595  g2a02-26f0-2f00-03b5-0000-0000-0000-097a:https  ESTABLISHED
 [coreServiceShell.exe]
  TCP    [2a01:e34:ef74:c9e0:a115:871a:4ea9:bbeb]:63639  [2600:9000:2156:2a00:2:39c7:7a00:21]:https  ESTABLISHED
 [chrome.exe]
  TCP    [2a01:e34:ef74:c9e0:a115:871a:4ea9:bbeb]:63656  g2a02-26f0-2f00-03b5-0000-0000-0000-097a:https  ESTABLISHED
 [coreServiceShell.exe]
  TCP    [2a01:e34:ef74:c9e0:a115:871a:4ea9:bbeb]:63662  imap:imaps             ESTABLISHED
  OneSyncSvc_3147f58
 [svchost.exe]
  UDP    0.0.0.0:427            *:*
  HPSLPSVC
 [svchost.exe]
  UDP    0.0.0.0:5050           *:*
  CDPSvc
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*
 [chrome.exe]
  UDP    0.0.0.0:5353           *:*
 [chrome.exe]
  UDP    0.0.0.0:5355           *:*
  Dnscache
[...]
```
* expliquer la fonction de chacun de ces programmes
```
A faire 
```

# III. Gestion de softs
Tous les OS modernes sont équipés ou peuvent être équipés d'un gestionnaire de paquets. Par exemple :
* `apt` pour les GNU/Linux issus de Debian
* `dnf` pour les GNU/Linux issus de RedHat
* `brew` pour macOS
* `chocolatey` pour Windows

Expliquer l'intérêt de l'utilisation d'un gestionnaire de paquets
* par rapport au téléchargement en direct sur internet
* penser à l'identité des gens utilisés dans un téléchargement (vous, éditeur logiciel, etc.)
* penser à la sécurité globale impliquée lors d'un téléchargement
```
Un gestionnaire de paquet permet l'installation de paquets à partir de l'invite de commande. Tous les paquets sont stockés dans le même dépôt. Cela permet à l'utilisateur d'avoir directement accès au paquets à installer. Le téléchargement se fait directement depuis le site demandé, il n'y a pas d'autres intervenants tel que le moteur de recherche. Ces paquets sont dépouvus de spywares et de malwares, ce qui assure la sécurité de ces derniers.
```

Utiliser un gestionnaire de paquet propres à votre OS pour
* lister tous les paquets déjà installés
```
PS C:\WINDOWS\system32> choco list --localonly
Chocolatey v0.10.15
chocolatey 0.10.15
chocolatey-core.extension 1.3.5.1
Firefox 82.0.2
3 packages installed.
```
* déterminer la provenance des paquets (= quel serveur nous délivre les paquets lorsqu'on installe quelque chose)
```
PS C:\WINDOWS\system32> choco source
Chocolatey v0.10.15
chocolatey - https://chocolatey.org/api/v2/ | Priority 0|Bypass Proxy - False|Self-Service - False|Admin Only - False.
```
















